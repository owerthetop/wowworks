<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m171123_062232_create_table_file
 */
class m171123_062232_create_table_file extends Migration
{

    public function up()
    {
        $this->createTable('files', [
            'doc_id' => Schema::TYPE_STRING,
            'name_pdf' => Schema::TYPE_STRING,
            'name_jpg' => Schema::TYPE_STRING,
            'way_jpg' => Schema::TYPE_STRING,
            'way_pdf' => Schema::TYPE_STRING,
        ]);
    }

    public function down()
    {
        $this->dropTable('files');
    }

}
