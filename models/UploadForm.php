<?php

namespace app\models;

use yii\base\Model;
use yii\web\HttpException;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file',
                'skipOnEmpty' => false,
                'extensions' => 'pdf',
                'maxFiles' => 20,
                'maxSize' => 52428800],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $docId = self::getNewId();
            $_SESSION['id'] = $docId;

            foreach ($this->imageFiles as $file) {


                //путь до pdf
                $wayPdf = "/uploads/{$docId}/pdf/";
                $wayPdf .= $file->baseName . '.';
                $wayPdf .= $file->extension;

                //имена файлов
                $pdfName = $file->baseName . '.' . $file->extension;
                $jpgName = $file->baseName . '.' . 'jpg';

                //путь до jpg
                $wayJpg = "/uploads/{$docId}/jpg/";
                $wayJpg .= $file->baseName . '.';
                $wayJpg .= 'jpg';

                //добавил проверку на директорию pdf
                if (is_dir($_SERVER['DOCUMENT_ROOT'] . "/uploads/{$docId}/pdf/")) {
                }
                else {
                    if (!mkdir($_SERVER['DOCUMENT_ROOT'] . "/uploads/{$docId}/pdf/", 0777, true)){
                        throw  new HttpException(1000, 'Не удалось создать директорию');
                    }
                }
                //добавил проверку на директорию jpg
                if (is_dir($_SERVER['DOCUMENT_ROOT'] . "/uploads/{$docId}/jpg/")){
                }else{
                    if (!mkdir($_SERVER['DOCUMENT_ROOT'] . "/uploads/{$docId}/jpg/", 0777, true)){
                        throw  new HttpException(1000, 'Не удалось создать директорию');
                    }
                }

                //сохраняем оригинал
                $file->saveAs($_SERVER['DOCUMENT_ROOT'] . $wayPdf);

                //сохраняем jpg
                $jpg = new \Imagick();
                $jpg->readImage($_SERVER['DOCUMENT_ROOT'] . $wayPdf);
                $jpg->writeImages($_SERVER['DOCUMENT_ROOT'] . $wayJpg, true);

                //сохраняем в БД
                $image = new Files();
                $image->doc_id = $docId;
                $image->name_jpg = $jpgName;
                $image->name_pdf = $pdfName;
                $image->way_jpg = $wayJpg;
                $image->way_pdf = $wayPdf;
                $image->save();
            }

            return true;
        } else {
            return false;
        }
    }

    public function getNewId()
    {

        $docId = "77АМ" . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9);

        $image = Files::findOne(['doc_id' => $docId]);


        for ($i=0; $image === null; $i++){
            if ($image === null){
                return $docId;
            }else{
                $docId = "77АМ" . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9);
                $image = Files::findOne(['doc_id' => $docId]);
            }
        }
        return $docId;
    }
}