<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Button;

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label('Загрузка PDF') ?>
<?= Button::widget([
    'label' => 'Конвертировать',
    'options' => [
        'class' => 'btn-small btn-success',
    ]
]); ?>

<?php ActiveForm::end() ?>