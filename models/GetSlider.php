<?php
namespace app\models;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.12.17
 * Time: 17:14
 */
class GetSlider extends Model
{
    public static function getSlider(){
        $wayImages = Files::find()->where(['doc_id' => $_SESSION['id']])->all();

        foreach ($wayImages as $key => $item) {
            $content[$key]['content'] = '<img src="' . $item->way_jpg . '"/>';
        }
        $carousel = $content;
        return $carousel;
    }
}