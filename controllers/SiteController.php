<?php

namespace app\controllers;

use app\models\Files;
use app\models\GetSlider;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use app\models\UploadForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{

    public function actionUpload()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');

            if ($model->upload()) {
                //получаем данные слайдера
                $carousel = GetSlider::getSlider();

                return $this->render('slider', ['carousel' => $carousel]);
            } else {
                throw new HttpException(1000, 'Dont worry, be Happy');
            }
        }

        return $this->render('upload', ['model' => $model]);
    }

    public function actionDownload()
    {
        $carousel = GetSlider::getSlider();

        return $this->render('slider', ['carousel' => $carousel]);
    }
}
