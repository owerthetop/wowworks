<?php
use yii\bootstrap\Carousel;
use yii\bootstrap\Button;
use yii\widgets\ActiveForm;
?>
<?= Carousel::widget([
    'items' => $carousel,
    'options' => ['class' => 'carousel slide', 'data-interval' => '12000'],
    'controls' => [
        '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
        '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
    ]
]);
?>

<?php $form = ActiveForm::begin(['action' => '/site/download']) ?>
<?= Button::widget([
    'label' => 'Скачать',
    'options' => [
        'class' => 'btn-small btn-success'
    ]
]);
?>
<?php ActiveForm::end() ?>
